terraform {
  backend "s3" {
    key    = "cluster"
    region = "sa-east-1"
    workspace_key_prefix = "tf"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
