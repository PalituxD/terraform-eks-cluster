
## Overview

This project creates an infrastruture that contains:
- 1 eks cluster.
- 2 ec2 as nodes for the cluster created.

## Pre-requisites
- terraform 1.1.7 and above (https://www.terraform.io/downloads)
- 1 enabled key for connecting to the AWS services.
- 1 bucket for terraform backend (use init.sh file for creating needed resources)
- 1 dynamodb for terraform backend (use init.sh file for creating needed resources)

## Installation
### Gitlab
- At gitlab -> Settings -> CI/CD, create variables below according to your environment:
    - AWS_ACCESS_KEY_ID: AWS credentials
    - AWS_SECRET_ACCESS_KEY: AWS credentials
    - TERRAFORM_BACKEND_BUCKET: Terraform state storage
    - TERRAFORM_BACKEND_TABLE: Terraform state storage
    - TF_VAR_sg_id_0: security group to be used by created nodes
    - TF_VAR_sn_id_0, TF_VAR_sn_id_1, TF_VAR_sn_id_2: subnets to be used by created nodes

More information about:
- TF_VAR_ vars: https://www.terraform.io/cli/config/environment-variables#tf_var_name


## Test and Deploy
### Gitlab
- When a branch is either created or updated, gitlab executes validate_plan job
- When the main branch is updated, gitlab:
    -  executes the validate_plan job.
    -  dev job is executed automatically 
    -  prd job is enabled for being executed manually. 

- After deploy you will find the cluster name in terraform output:

Apply complete! Resources: 12 added, 0 changed, 0 destroyed.
   `
    Outputs:
    eks_cluster_endpoint = "https://EE85929F21167948AFBAD66765571B63.gr7.sa-east-1.eks.amazonaws.com"
    eks_cluster_name = "eks-cluster-dev"
   `

## Connect to the cluster
- Run: aws eks update-kubeconfig --region sa-east-1 --name [eks_cluster_name]

## Install aws-ebs-csi-driver
- Run: kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT License
