# set values for BUCKET_NAME and TABLE_NAME
export BUCKET_NAME=
export TABLE_NAME=

aws s3api create-bucket --bucket $BUCKET_NAME --region sa-east-1

aws dynamodb create-table \
--table-name $TABLE_NAME \
--attribute-definitions AttributeName=LockID,AttributeType=S \
--key-schema AttributeName=LockID,KeyType=HASH \
--billing-mode PAY_PER_REQUEST \
--region sa-east-1 \