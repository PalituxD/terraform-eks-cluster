variable "cluster_name" {
  default = "eks-cluster"
  type    = string
}

resource "aws_eks_cluster" "eks_cluster" {
  depends_on = [aws_cloudwatch_log_group.cluster_log_group]
  role_arn = aws_iam_role.cluster_role.arn
  enabled_cluster_log_types = ["api", "audit"]
  name                      = "${var.cluster_name}-${terraform.workspace}"
  vpc_config {
    endpoint_public_access = true
    security_group_ids = [var.sg_id_0]
    subnet_ids = [var.sn_id_0, var.sn_id_1, var.sn_id_2]
  }
}

resource "aws_cloudwatch_log_group" "cluster_log_group" {
  name              = "/aws/eks/${var.cluster_name}"
  retention_in_days = 7
}

resource "aws_iam_role" "cluster_role" {
  name = "eks-cluster-iam-role-${terraform.workspace}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cluster_role-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster_role.name
}


resource "aws_iam_role_policy_attachment" "cluster_role-AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.cluster_role.name
}

output "eks_cluster_endpoint" {
    value = aws_eks_cluster.eks_cluster.endpoint
}

output "eks_cluster_name" {
    value = aws_eks_cluster.eks_cluster.name
}
